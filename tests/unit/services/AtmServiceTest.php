<?php

namespace tests\unit\models;

use app\models\Atm;
use app\models\AtmNominateBanknote;
use app\services\AtmService;
use Codeception\Stub\Expected;
use Money\Money;

class AtmServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;
    const REAL_ATM_ID = 1;

    private function getAtm()
    {
        return \Yii::$app->atm->getAtmById(1);
    }

    private function getService()
    {
        return $service = new AtmService(1);
    }

    private function assertAtmNominate($banknotes, $atmId = self::REAL_ATM_ID)
    {
        foreach ($banknotes as $nominateId => $count) {
            $this->tester->seeRecord(AtmNominateBanknote::class, [
                'nominate_banknote_id' => $nominateId,
                'count' => $count,
                'atm_id' => $atmId
            ]);
        }
    }

    public function testSuccessGetInstance()
    {
        $service = new AtmService();

        $instanceWithAtm = $service->getAtmById(self::REAL_ATM_ID);
        $atmInst = $instanceWithAtm->getAtmInstance();
        $this->assertInstanceOf(AtmService::class, $instanceWithAtm);
        $this->assertInstanceOf(Atm::class, $atmInst);
    }

    public function testSuccessSameInstance()
    {
        $mock = $this->construct(AtmService::class, []);
        $atm = $mock->getAtmById(self::REAL_ATM_ID);
        $otherAtm = $mock->getAtmById(self::REAL_ATM_ID);
        $this->assertEquals($atm, $otherAtm);
    }

    public function testFailGetAtmInstance()
    {
        $service = new AtmService();
        $atm = $service->getAtmById(2);
        $instance = $atm->getAtmInstance();
        $this->assertNull($instance);
    }

    public function testConvertToAtmCurrency()
    {
        $atm = $this->getAtm();

        $result = $atm->convertToAtmCurrency(500);
        $this->assertInstanceOf(Money::class, $result);
        $this->assertEquals($result->getAmount(), 500);
        $this->assertEquals($result->getCurrency(), 'RUB');
    }

    public function testSuccessFill()
    {
        $result = $this->getService()
            ->fill([
                '500' => 5,
                '100' => 1,
                '10' => 100,
                '15' => 99
            ]);

        $this->assertTrue($result);
        $this->assertAtmNominate([
            '3' => 5,
            '2' => 1,
            '1' => 100
        ]);

        $this->tester->cantSeeRecord(AtmNominateBanknote::class, [
            'count' => 99
        ]);
    }

    public function testSuccessFillDouble()
    {
        $atm = $this->getService();
        $atm->fill([
            '500' => 6,
            '10' => 97,
        ]);

        $atm->fill([
            '500' => 3,
            '10' => 2,
        ]);

        $this->assertAtmNominate([
            '3' => 9,
            '1' => 99
        ]);
    }

    public function testSuccessWithdrawal()
    {
        $atm = $this->getService();

        $atm->fill([
            '500' => 3,
            '100' => 4,
            '10' => 2,
        ]);

        $amount = $atm->convertToAtmCurrency(161000);
        $atm->withdrawal($amount);
        $this->assertAtmNominate([
            '3' => 0,
            '2' => 3,
            '1' => 1
        ]);
    }

    public function failWithdrawalBigSum()
    {
        $atm = $this->getService();

        $atm->fill([
            '500' => 3,
            '100' => 4,
            '10' => 2,
        ]);

        $amount = $atm->convertToAtmCurrency(10000);
        $result = $atm->withdrawal($amount);
        $this->isFalse($result);
        $this->assertAtmNominate([
            '3' => 3,
            '2' => 4,
            '1' => 2
        ]);
    }

    public function testStatistic()
    {
        $atm = $this->getService();
        $successFill = $atm->fill([
            '500' => 3,
            '100' => 4,
            '10' => 2,
        ]);
        $amount = $atm->convertToAtmCurrency(70000);
        $successWithdrawal = $atm->withdrawal($amount);
        $this->assertTrue($successFill);
        $this->assertTrue($successWithdrawal);

        $statistic = $atm->getStatistics();
        $expected = [[
            'type' => 'up',
            'amount' => 192000,
            'currency' => 'RUB'
        ], [
            'type' => 'down',
            'amount' => 70000,
            'currency' => 'RUB'
        ]];

        foreach ($expected as $index => $item) {
            $this->assertEquals($item['type'], $statistic[$index]['type']);
            $this->assertEquals($item['amount'], $statistic[$index]['amount']->getAmount());
            $this->assertEquals($item['currency'], $statistic[$index]['amount']->getCurrency());
        }
    }

    public function testSuccessBalance()
    {
        $atm = $this->getService();
        $successFill = $atm->fill([
            '500' => 6,
            '100' => 7,
            '10' => 15,
        ]);

        $balance = $atm->getBalance();

        $this->assertTrue($successFill);
        $this->assertEquals(385000, $balance->getAmount());
    }

    public function testSuccessNotes()
    {
        $expected = [
            '500' => 6,
            '100' => 7,
            '10' => 15,
        ];

        $atm = $this->getService();
        $successFill = $atm->fill($expected);

        $notes = $atm->getNotes();
        $this->assertTrue($successFill);
        $this->assertEquals($expected, $notes);


    }
}
