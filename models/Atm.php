<?php

namespace app\models;

use Yii;
use app\models\AtmInterface;
use app\models\AtmNominateBanknote;
use app\models\NominateBanknote;
use app\models\AtmOperation;
use app\models\Currency;
use app\enums\AtmOperationEnum;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\db\Expression;
use Money\Currency as MoneyCurrency;
use Money\Money;

/**
 * This is the model class for table "atm".
 *
 * @property int $id
 * @property string $address
 * @property int $currency_id
 *
 * @property Currency $currency
 */
class Atm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['currency_id'], 'required'],
            [['currency_id'], 'integer'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'currency_id' => 'Currency ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtmNominateBanknotes()
    {
        return $this->hasMany(AtmNominateBanknote::className(), ['atm_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtmNominateBanknote($nominalId)
    {
        return $this->hasOne(AtmNominateBanknote::className(), ['atm_id' => 'id'])
            ->where(['nominate_banknote_id' => $nominalId]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominates()
    {
        return $this->hasMany(NominateBanknote::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtmOperations()
    {
        return $this->hasMany(AtmOperation::className(), ['atm_id' => 'id']);
    }
}
