<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nominate_banknote".
 *
 * @property int $id
 * @property int $currency_id
 * @property int $count
 */
class NominateBanknote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nominate_banknote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_id', 'count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_id' => 'Currency ID',
            'count' => 'Count',
        ];
    }
}
