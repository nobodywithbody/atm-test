<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "atm_operation".
 *
 * @property int $id
 * @property int $atm_id
 * @property string $type
 * @property int $sum
 * @property string $created_at
 */
class AtmOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm_operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['atm_id', 'sum'], 'integer'],
            [['type'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'atm_id' => 'Atm ID',
            'type' => 'Type',
            'sum' => 'Sum',
            'created_at' => 'Created At',
        ];
    }
}
