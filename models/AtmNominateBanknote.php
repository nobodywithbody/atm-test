<?php

namespace app\models;

use Yii;
use app\models\NominateBanknote;

/**
 * This is the model class for table "atm_nominate_banknote".
 *
 * @property int $id
 * @property int $atm_id
 * @property int $nominate_banknote_id
 * @property int $count
 */
class AtmNominateBanknote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm_nominate_banknote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['atm_id', 'nominate_banknote_id', 'count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'atm_id' => 'Atm ID',
            'nominate_banknote_id' => 'Nominate Banknote ID',
            'count' => 'Count',
        ];
    }

    /**
      * @return \yii\db\ActiveQuery
      */
    public function getNominateBanknote()
    {
        return $this->hasOne(NominateBanknote::className(), ['id' => 'nominate_banknote_id']);
    }
}
