<?php

namespace app\enums;

class BaseEnum
{
    public static function getList()
    {
        $reflect = new \ReflectionClass(static::class);
        $constats = $reflect->getConstants();

        return array_keys($constats);
    }
}
