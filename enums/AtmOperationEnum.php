<?php

namespace app\enums;

use app\enums\BaseEnum;

class AtmOperationEnum extends BaseEnum
{
    const UP = 'UP';
    const DOWN = 'DOWN';
}
