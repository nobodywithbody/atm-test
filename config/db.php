<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=atm',
    'username' => 'root',
    'password' => getenv('DB_PASS'),
    'charset' => 'utf8',
];
