<?php

namespace app\controllers;

use app\services\AtmService;
use Yii;
use yii\web\Controller;

class AtmController extends Controller
{
    public function actionPostFill($id)
    {

        $form = Yii::$app->request->post();
        if (!isset($form['banknotes'])) {
            return ['result' => false];
        }
        $result = Yii::$app->atm->getAtmById($id)
            ->fill($form['banknotes']);
        
        return compact('result');
    }

    public function actionPostWithdrawal($id)
    {
        $form = Yii::$app->request->post();
        /** @var AtmService $atm */
        $atm = Yii::$app->atm->getAtmById($id);
        $money = $atm->convertToAtmCurrency($form['amount']);
        $result = $atm->withdrawal($money);
        
        return compact('result');
    }

    public function actionGetStatistic($id)
    {
        $result = Yii::$app->atm->getAtmById($id)
            ->getStatistics();

        return compact('result');
    }

    public function actionGetBalance($id)
    {
        $result = Yii::$app->atm->getAtmById($id)
            ->getBalance();

        return compact('result');
    }

    public function actionGetNotes($id)
    {
        $result = Yii::$app->atm->getAtmById($id)
            ->getNotes();

        return compact('result');
    }
}
