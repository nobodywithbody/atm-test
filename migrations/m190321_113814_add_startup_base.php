<?php

use yii\db\Migration;
use app\enums\AtmOperationEnum;

/**
 * Class m190321_113814_add_startup_base
 */
class m190321_113814_add_startup_base extends Migration
{
    const CURRENCY_TABLE = 'currency';
    const ATM_TABLE = 'atm';
    const ATM_OPERATION_TABLE = 'atm_operation';
    const NOMINATE_BANKNOTE_TABLE = 'nominate_banknote';
    const ATM_CURRENCY_ID_FOREIGN_KEY = 'atm_currency_id_currency_id';
    const ATM_NOMINATE_BANKNOTE_TABLE = 'atm_nominate_banknote';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $operationTypes = join(AtmOperationEnum::getList(), "','");

        $this->createTable(self::CURRENCY_TABLE, [
            'id' => $this->primaryKey(),
            'code' => $this->char(3),
        ]);

        $this->addCommentOnTable(self::CURRENCY_TABLE, 'List of currencies');
        $this->addCommentOnColumn(self::CURRENCY_TABLE, 'code', 'Code of currency');

        $this->createTable(self::ATM_TABLE, [
            'id' => $this->primaryKey(),
            'address' => $this->text(),
            'currency_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(self::ATM_CURRENCY_ID_FOREIGN_KEY,
            self::ATM_TABLE,
            'currency_id',
            self::CURRENCY_TABLE,
            'id'
        );

        $this->createTable(self::ATM_OPERATION_TABLE, [
            'id' => $this->primaryKey(),
            'atm_id' => $this->bigInteger(),
            'type' => "ENUM('$operationTypes')",
            'sum' => $this->bigInteger(),
            'created_at' => 'datetime(0) DEFAULT CURRENT_TIMESTAMP()'
        ]);

        $this->createTable(self::NOMINATE_BANKNOTE_TABLE, [
            'id' => $this->primaryKey(),
            'currency_id' => $this->bigInteger(),
            'count' => $this->integer(),
        ]);

        $this->createTable(self::ATM_NOMINATE_BANKNOTE_TABLE, [
            'id' => $this->primaryKey(),
            'atm_id' => $this->bigInteger(),
            'nominate_banknote_id' => $this->bigInteger(),
            'count' => $this->integer()
        ]);

        $this->insert(self::CURRENCY_TABLE, [
            'code' => 'RUB'
        ]);

        $this->insert(self::ATM_TABLE, [
            'address' => 'Somewhere in Moscow',
            'currency_id' => 1
        ]);

        $this->batchInsert(self::NOMINATE_BANKNOTE_TABLE, ['currency_id', 'count'], [[
            1, 10
        ], [
            1, 100
        ], [
            1, 500
        ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(self::ATM_CURRENCY_ID_FOREIGN_KEY, self::ATM_TABLE);
        $this->dropTable(self::ATM_TABLE);
        $this->dropTable(self::CURRENCY_TABLE);
        $this->dropTable(self::ATM_OPERATION_TABLE);
        $this->dropTable(self::NOMINATE_BANKNOTE_TABLE);
        $this->dropTable(self::ATM_NOMINATE_BANKNOTE_TABLE);
    }
}
