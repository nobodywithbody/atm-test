<?php

namespace app\services;

use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Yii;
use app\models\Atm;
use app\models\AtmNominateBanknote;
use app\models\AtmOperation;
use app\enums\AtmOperationEnum;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use Money\Currency as MoneyCurrency;
use Money\Money;

class AtmService implements AtmInterface
{
    private $atmId;
    private $atm = null;

    public function __construct($id = null)
    {
        $this->setAtmId($id);
    }

    private function createNominateFill($nominateId, $count)
    {
        $nominateAtmBanknote = new AtmNominateBanknote([
            'atm_id' => $this->atm->id,
            'nominate_banknote_id' => $nominateId,
            'count' => $count
        ]);

        $nominateAtmBanknote->save();
    }

    private function updateNominateFill(AtmNominateBanknote $atmNominate, $count)
    {
        $atmNominate->count = $atmNominate->count + $count;
        $atmNominate->update();
    }

    private function calcIncome(array $notes): int
    {
        return array_reduce(array_keys($notes), function ($acc, $cur) use ($notes) {
                return $acc + $notes[$cur] * $cur;
            }, 0) * 100;
    }

    private function setAtmId(int $atmId = null)
    {
        $this->atmId = $atmId;
    }

    public function getAtmInstance(): ?Atm
    {
        if (!$this->atmId) {
            return null;
        }

        if ($this->atm) {
            return $this->atm;
        }

        $atm = Atm::findOne($this->atmId);
        if (!$atm) {
            return null;
        }

        $this->atm = $atm;

        return $this->atm;
    }

    public function convertToAtmCurrency(int $amount): Money
    {
        if (!$atm = $this->getAtmInstance()) {
            throw new \Exception('no have ATM instance');
        }

        $currency = $atm->currency;
        $moneyCurrency = new MoneyCurrency($currency->code);

        return new Money($amount, $moneyCurrency);
    }

    /**
     * @param $id
     * @return AtmService
     */
    public function getAtmById($id)
    {
        $atm = $this->getAtmInstance();
        if ($atm && $atm->id === $id) {
            return $this;
        }

        return new static($id);
    }

    public function fill(array $notes): bool
    {
        if (!$atm = $this->getAtmInstance()) {
            return false;
        }

        /** @var AtmOperationService $atmOperationService */
        $atmOperationService = \Yii::$app->atmOperation;
        $nominateBanknotes = $atm->getNominates()
            ->andWhere(['in', 'count', array_keys($notes)])
            ->all();

        $nominateBanknotesMap = array_reduce($nominateBanknotes, function ($acc, $cur) {
            $acc[$cur->count] = $cur->id;
            return $acc;
        }, []);

        $sum = $this->calcIncome($notes);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($notes as $note => $count) {
                if (!isset($nominateBanknotesMap[$note])) {
                    continue;
                }
                $nominateId = $nominateBanknotesMap[$note];

                $atmNominate = $atm->getAtmNominateBanknote($nominateId)->one();
                if (!$atmNominate) {
                    $this->createNominateFill($nominateId, $count);
                    continue;
                }

                $this->updateNominateFill($atmNominate, $count);
            }
            $atmOperationService->fill($sum, $atm->id);
            $transaction->commit();
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), static::class);
            $transaction->rollback();
            return false;
        }


        return true;
    }

    private function formatMoney(Money $money): int
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        return floor($moneyFormatter->format($money));
    }

    public function withdrawal(Money $money): bool
    {
        if (!$atm = $this->getAtmInstance()) {
            return false;
        }
        /** @var AtmOperationService $atmOperationService */
        $atmOperationService = \Yii::$app->atmOperation;

        $nominates = $atm->getNominates()->orderBy(['count' => SORT_DESC])->all();
        $needWithdrawalNominate = [];
        $needDistributeAmount = $this->formatMoney($money);
        foreach ($nominates as $nominate) {
            $operationNominate = $atm->getAtmNominateBanknote($nominate->id)->one();
            $countOfNominate = ArrayHelper::getValue($operationNominate, 'count', 0);
            $totalAmountCurrentNominate = $countOfNominate * $nominate->count;
            if ($totalAmountCurrentNominate === 0) {
                continue;
            }

            if ($needDistributeAmount < $nominate->count) {
                continue;
            }

            $needDistribute = floor($needDistributeAmount / $nominate->count);
            $finalyDistribute = $countOfNominate < $needDistribute ? $countOfNominate : $needDistribute;
            $operationNominate->count -= $finalyDistribute;
            $needDistributeAmount -= $nominate->count * $finalyDistribute;
            $needWithdrawalNominate[] = $operationNominate;
        }

        if ((int)$needDistributeAmount !== 0) {
            return false;
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($needWithdrawalNominate as $needWithdrawal) {
                $needWithdrawal->update();
            }
            $atmOperationService->withdrawal($money->getAmount(), $atm->id);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            return false;
        }


        return true;
    }

    public function getStatistics(): array
    {
        if (!$atm = $this->getAtmInstance()) {
            return [];
        }

        return array_map(function ($el) {
            return [
                'type' => strtolower($el->type),
                'amount' => $this->convertToAtmCurrency($el->sum),
                'date' => new \DateTime($el->created_at)
            ];
        }, $atm->atmOperations);
    }

    public function getBalance(): Money
    {
        if (!$atm = $this->getAtmInstance()) {
            return null;
        }

        $query = $atm
            ->getAtmNominateBanknotes()
            ->joinWith('nominateBanknote');

        $sum = $query->sum(new Expression('atm_nominate_banknote.count * nominate_banknote.count * 100'));

        return $this->convertToAtmCurrency($sum);
    }

    public function getNotes(): array
    {
        if (!$atm = $this->getAtmInstance()) {
            return [];
        }

        $notes = $atm
            ->getAtmNominateBanknotes()
            ->with('nominateBanknote')
            ->all();

        return array_reduce($notes, function ($acc, $el) {
            $acc[$el->nominateBanknote->count] = $el->count;
            return $acc;
        }, []);
    }
}
