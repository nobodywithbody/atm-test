<?php

namespace app\services;

use Money\Money;

interface AtmInterface
{
    /**
     * Загрузка наличных средств в банкомат
     * $notes = [
     *     // Номинал купюры => Количество
     *     100 => 25,
     *     5000 => 10,
     * ];
     */
    public function fill(array $notes): bool;

    /**
     * Выдача наличных средств. Выдавать запрошенную сумму имеющимися купюрами,
     * отдавая приоритет более крупным банкнотам
     *
     * @param Money $amount
     * @return bool
     */
    public function withdrawal(Money $amount): bool;

    /**
     * Получение статистики
     * $statistics = [
     *     ['type' => 'up', 'amount' => Money(amount, currency), 'date' => DateTime(date)],
     *     ['type' => 'down', 'amount' => Money(amount, currency), 'date' => DateTime(date)],
     * ];
     */
    public function getStatistics(): array;

    /**
     * Текущий баланс банкомата
     */
    public function getBalance(): Money;

    /**
     * Получение информации о купюрах в кассете банкомата
     * $notes = [
     *     // Номинал купюры => Количество
     *     100 => 25,
     *     5000 => 10,
     * ];
     */
    public function getNotes(): array;
}
