<?php
/**
 * Created by PhpStorm.
 * User: nobodywithbody
 * Date: 24.03.19
 * Time: 10:51
 */

namespace app\services;


use app\enums\AtmOperationEnum;
use app\models\AtmOperation;
use yii\base\Component;

class AtmOperationService extends Component
{
    public function fill($amount, $atmId)
    {
        $operation = new AtmOperation([
            'sum' => $amount,
            'atm_id' => $atmId,
            'type' => AtmOperationEnum::UP
        ]);

        return $operation->save();
    }

    public function withdrawal($amount, $atmId)
    {
        $operation = new AtmOperation([
            'sum' => $amount,
            'atm_id' => $atmId,
            'type' => AtmOperationEnum::DOWN
        ]);

        return $operation->save();
    }
}